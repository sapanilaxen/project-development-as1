Hi there! Laxen Sapani here

This is ASP.Net 2.2 Music Store Website.

-To deploy the project, open repository and extract it.

-Execute MVCMusicStore.sql file in SQL Server Management Studio to create database and keep it running.

-Open .sln file in Visual Studio 2017 or any lower version.

-Open appsetting.json file and modify the server name.

-That's it. You are goood to go.

For License file, I used GNU General Public License v3.0 because I believe in sharing and distributing. 