﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MvcMusicStore.Models;

namespace MvcMusicStore.Controllers
{
    public class StoreController : Controller
    {
        public IActionResult AlbumList()
        {
            List<Album> albums = new List<Album>();
            for(int i=0; i<10; i++)
            {
                albums.Add(new Album { Title = "Album" + i });
            }
            ViewBag.Albums = albums;
            return View();
        }
        public IActionResult Sample()
        {
            ViewBag.message = "this is the ViewBag property 'message'";
            return View();
        }
        public string Index()
        {
            return "Hello from Store Index";
        }
        public string Browse(string genre)
        {
            return "Hello from Browse in Store " + genre;
        }
        public string Details(int id)
        {
            return "Hello from Details in Store " + id.ToString();
        }
    }
}